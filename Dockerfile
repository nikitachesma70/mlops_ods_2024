FROM mambaorg/micromamba

WORKDIR /app


COPY env.yml /app/env.yml
RUN micromamba create -f env.yml

COPY pyproject.toml pdm.lock /app/
RUN micromamba run -n my_venv pdm install --check --no-self

COPY . .

# CMD ["micromamba", "run", "-n", "my_venv","pdm", "run", "python", "main.py"]
